---
title: Registration is now open for DebConf18, in Hsinchu, Taiwan
---

We are delighted and proud to announce that you can now register for DebConf18!

DebConf18 will take place in Hsinchu, Taiwan from 29 July to 5 August 2018.
It will be preceded by DebCamp, July 21 to July 27, and Open Day, July 28.

To register for DebConf18, visit our website at
[https://debconf18.debconf.org/register/][reg-site].

To request bursaries (sponsorship) for food, accommodation, or travel, you must
be registered by Friday, 13 April 2018. After this date, new bursary applications
won’t be considered.

Participation to DebConf18 is conditional to your respect of our
[Code of Conduct][coc]. We require you to read, understand and abide by this
code.

Even if you are not certain you will be able to attend, we recommend
registering now. You can always edit or cancel your registration, but please make it
final before 21 June 2018. We cannot guarantee availability of accommodation,
catering or swag for unconfirmed registrations.

We do suggest that attendees begin making travel arrangements as soon as
possible, of course.

Any questions about registrations should be addressed to
[registration@debconf.org][reg-mail].


## Travel to Taiwan

Please refer to the visa information webpage from [MeetTaiwan][meet-taiwan]
to see if you need a visa or equivalent to visit Taiwan.
See our [visa information page][visa] for more details.


## Thank you

The DebConf18 team would like to thank all of our sponsors who make this
event possible:

[https://debconf18.debconf.org/sponsors/][sponsors]

We are still seeking sponsors to help us make DebConf18 a success. If
you or your company would like to give back to Debian, please consider
becoming a sponsor.

We look forward to seeing you in Hsinchu!

The DebConf team

[reg-site]: https://debconf18.debconf.org/register/
[coc]: https://debconf.org/codeofconduct.shtml
[reg-mail]: mailto:registration@debconf.org
[sponsors]: https://debconf18.debconf.org/sponsors/
[meet-taiwan]: https://www.meettaiwan.com/en_US/menu/M0000795/General%20Information.html
[visa]: https://debconf18.debconf.org/about/visiting-hsinchu/


