from django.contrib import admin

from register.admin.core import NotNullFilter
from register.models.attendee import Attendee


class NotesNotNullFilter(NotNullFilter):
    title = "Notes"
    parameter_name = "notes"


class AttendeeAdmin(admin.ModelAdmin):
    list_display = (
        'user', 'full_name', 'email', 'reconfirm', 'arrival', 'departure',
        'billable', 'paid',
    )
    list_filter = (
        'fee', 'arrival', 'departure', 'reconfirm', 't_shirt_cut',
        't_shirt_size', NotesNotNullFilter,
    )
    search_fields = ('user__username', 'user__first_name', 'user__last_name')
    readonly_fields = ('full_name', 'email')

    def full_name(self, instance):
        return instance.user.get_full_name()
    full_name.admin_order_field = 'user__last_name'

    def email(self, instance):
        return instance.user.email
    email.admin_order_field = 'user__email'


admin.site.register(Attendee, AttendeeAdmin)
